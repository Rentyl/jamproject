﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    private Collider2D col;
    void Start()
    {
        col = GetComponent<Collider2D>();
    }


    void OnEnable()
    {
        Static.TaxiIsArrived += DoorOpen;
    }

    void OnDisable()
    {
        Static.TaxiIsArrived -= DoorOpen;
    }

    void DoorOpen()
    {
        col.isTrigger = true;
    }

    void Update()
    {
        
    }
}
