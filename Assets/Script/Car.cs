﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    private Rigidbody2D _rd;
    [SerializeField]
    private Transform pointDest;

    private Vector2 startPos;
    private SpriteRenderer _sr;
    [SerializeField] 
    private Sprite redSprite;

    void Start()
    {
        _rd = GetComponent<Rigidbody2D>();
        _sr = GetComponent<SpriteRenderer>();
        startPos = _rd.position;
    }

    void OnEnable()
    {
        Static.TaxiIsArrived += ToArrive;
        Static.TaxiIsLeave += ToLeave;
        Static.ParentsIsArrived += ToArrive;
    }

    void OnDisable()
    {
        Static.TaxiIsArrived -= ToArrive;
        Static.TaxiIsLeave -= ToLeave;
        Static.ParentsIsArrived -= ToArrive;
    }

    private bool arrive;
    void ToArrive()
    {
        StartCoroutine(Arrive());
    }

    IEnumerator Arrive()
    {
        float timer = 0;
        while (timer < 1)
        {
            timer += Time.deltaTime;
            _rd.position = new Vector2(Mathf.Lerp(_rd.position.x, pointDest.position.x, timer), _rd.position.y);
            yield return null;
        }
    }


    void ToLeave()
    {
        StartCoroutine(Leave());
    }

    IEnumerator Leave()
    {
        float timer = 0;
        while (timer < 1)
        {
            timer += Time.deltaTime;
            _rd.position = new Vector2(Mathf.Lerp(_rd.position.x, startPos.x, timer), _rd.position.y);
            yield return null;
        }

        _sr.sprite = redSprite;
        _sr.flipX = false;
    }

    void Update()
    {
        if (arrive)
        {

        }
    }
}
