﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Manager : MonoBehaviour
{

    [SerializeField] 
    private AudioSource music;
    [SerializeField]
    private GameObject startScreen;
    [SerializeField] 
    private GameObject victoryUI;

    [SerializeField] 
    private GameObject defeatUI;
    [SerializeField]
    List<Furniture> furnituresList = new List<Furniture>();
    [SerializeField]
    List<ScratchCardManager> scratchManager = new List<ScratchCardManager>();
    [SerializeField]
    private Transform[] textDefeat;
	
	
    private bool defeat;
    private bool victory;
    void OnEnable()
    {
        Static.TimerIsOut += CheckFurnitureState;
    }
    void OnDisable()
    {
        Static.TimerIsOut -= CheckFurnitureState;
    }

    private int count;

    void StopMusic()
    {
        music.Pause();
    }
    void CheckFurnitureState()
    {
        StopMusic();
        foreach (var v in furnituresList)
        {
            if (v.isPlace)
            {
                count++;
            }
        }

        if (count == 5)
        {
            Static.furniture = true;
            textDefeat[0].gameObject.SetActive(false);
        }
        CheckGarbage();
    }

    void Defeat()
    {
        defeatUI.SetActive(true);
    }

    IEnumerator Wait()
    {
        yield return new WaitForSecondsRealtime(1f);
        CheckFinal();
    }
	
	
    void CheckFinal()
    {
        if (Static.furniture && Static.garbageCount == 0 && Static.friend == 0 && picture)
        {
            Victory();
        }
        else
        {
            Defeat();
        }
    }
	
    void Victory()
    {
        victoryUI.SetActive(true);
    }

    void CheckGarbage()
    {
        if (Static.garbageCount == 0)
        {
            
            textDefeat[2].gameObject.SetActive(false);

        }
        CheckFriends();
    }

    void CheckFriends()
    {
        if (Static.friend == 0)
        {
            textDefeat[3].gameObject.SetActive(false);

        }
        CheckPicture();
    }
    private int countPictureProgress;
    private bool picture;
    void CheckPicture()
    {
        for (int i = 0; i < scratchManager.Count; i++)
        {
            if (scratchManager[i].Progress.GetProgress() >= 0.95f)
            {
                Static.picture--;
                print(Static.picture + " " + scratchManager[i].Progress.GetProgress() + " " + i);
            }
        }
        if (Static.picture == 0)
        {
            picture = true;
            textDefeat[1].gameObject.SetActive(false);
        }
        StartCoroutine(Wait());
    }
    
    void Start()
    {
        startScreen.SetActive(true);
    }


    public void Reload()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        
        Static.ResetAll();
    }

    public void StartGame()
    {
        Static.pause = false;

    }
}
