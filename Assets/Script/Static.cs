﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Static : MonoBehaviour
{

    public static bool isControlActive;
    public static bool furniture;
    public static int garbageCount = 6;
    public static int friend = 3;
    public static int picture = 3;
    public static bool pause = true;

    public delegate void TimerOut();
    public static event TimerOut TimerIsOut;

    public delegate void TaxiArrive();
    public static event TaxiArrive TaxiIsArrived;
    public delegate void TaxiLeave();
    public static event TaxiLeave TaxiIsLeave;

    public delegate void ParentsArrive();
    public static event ParentsArrive ParentsIsArrived;
    

    public delegate void SoundEnable();
    public static event SoundEnable SoundOn;

    public delegate void ResetRD();
    public static event ResetRD Reset_RD;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public static void ResetAll()
    {
        isControlActive = false;
        furniture = false;
        garbageCount = 6;
        friend = 3;
        picture = 3;
        pause = true;
    }

    public static void EventTimerOut()
    {
        if (TimerIsOut != null)
            TimerIsOut();
    }
    public static void EventResetRD()
    {
        if (Reset_RD != null)
            Reset_RD();
    }
    public static void EventTaxiArrived()
    {
        if (TaxiIsArrived != null)
            TaxiIsArrived();
    }
    public static void EventTaxiLeaved()
    {
        if (TaxiIsLeave != null)
            TaxiIsLeave();
    }
    public static void EventParentsArrived()
    {
        if (ParentsIsArrived != null)
            ParentsIsArrived();
    }

    public static void EventSoundEnable()
    {
        if (SoundOn != null)
            SoundOn();
    }
}
