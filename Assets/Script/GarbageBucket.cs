﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GarbageBucket : MonoBehaviour
{
    void Start()
    {
        
    }


    void Update()
    {
        
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Garbage"))
        {
            //Static.EventResetRD();
            col.gameObject.SetActive(false);
            Static.garbageCount--;
        }
    }
}
