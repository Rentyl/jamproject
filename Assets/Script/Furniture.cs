﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Furniture : MonoBehaviour
{
    
    [SerializeField]
    private Transform furnitureObj;
    [SerializeField]
    float distance;
    [SerializeField]
    private SpriteRenderer sprite;



    public bool isPlace;
    void Start()
    {
        //sprite = GetComponent<SpriteRenderer>();
    }


    void Update()
    {
        if (!Static.isControlActive)
        {
            
            if (Vector2.Distance(furnitureObj.position, transform.position) < distance 
                && furnitureObj.eulerAngles.z > -10 && furnitureObj.eulerAngles.z < 10)
            {
                sprite.color = Color.green;
                //Static.garbageCount +
                isPlace = true;

            }
            else
            {
                sprite.color = Color.red;
                isPlace = false;
            }
        }



    }
}
