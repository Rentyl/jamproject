﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;


public class Control : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI timerText;
    // Start is called before the first frame update
    void Start()
    {
        camera = Camera.main;
        cameraTs = camera.gameObject.transform;
    }
    
    [SerializeField]
    List<ScratchCard> scratch = new List<ScratchCard>();
    private RaycastHit2D raycast;
    // Update is called once per frame
    private Vector2 clickPoint;
    private Vector2 firstPoint;
    private bool firstClick;
    private Vector2 offset;
    Rigidbody2D _rd = null;
    private Friend currentFriend;
    private bool friend;
    private bool isRaycast;
    private Camera camera;
    private Transform cameraTs;
    [SerializeField]
    private float cameraSpeed;

    [SerializeField]
    private float leftClamp;
    [SerializeField] 
    private float rightClamp;

    private float timer = 60;
    private int timerInt;
    private bool blockTaxiArrived;
    private bool blockTaxiLeaved;
    private bool blockParentsArrived;
    private bool blockTimer;

    private Vector2 cameraClick;
    void Timer()
    {
        timer -= Time.deltaTime;
        timerInt = (int) timer;
        timerText.text = timerInt.ToString();
        if (timer <= 30 && !blockTaxiArrived)
        {
            Static.EventTaxiArrived();
            blockTaxiArrived = true;
        }
        if (timer <= 10 && !blockTaxiLeaved)
        {
            Static.EventTaxiLeaved();
            blockTaxiLeaved = true;
        }
        if (timer <= 5 && !blockParentsArrived)
        {
            Static.EventParentsArrived();
            blockParentsArrived = true;
        }
        if (timer <= 0 && !blockTimer)
        {
            Static.EventTimerOut();
            blockTimer = true;
        }
    }

    Vector3 touchStart;
    public float zoomOutMin = 7.77f;
    public float zoomOutMax = 9.4f;


    void FixedUpdate()
    {
        if(!blockTimer && !Static.pause)
            Timer();

        if (Input.GetMouseButtonDown(0))
        {
            touchStart = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            MouseDown();
        }
        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float currentMagnitude = (touchZero.position - touchOne.position).magnitude;

            float difference = currentMagnitude - prevMagnitude;

            Zoom(difference * 0.01f);
        }
        if (Input.GetMouseButton(0))
        {
            
            MouseDrag();
        }
        if (Input.GetMouseButtonUp(0))
        {
            Static.isControlActive = false;
            if (friend)
            {
               
            }
            else
            {
                if(_rd !=null)
                    _rd.velocity = Vector2.zero;
            }
            foreach (var v in scratch)
            {
                v.InputEnabled = false;
            }
            _rd = null;
            isRaycast = false;
            firstClick = false;
            friend = false;
            clickPoint = Vector2.zero;
        }
        Zoom(Input.GetAxis("Mouse ScrollWheel"));
    }



    void Zoom(float increment)
    {
        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize - increment, zoomOutMin, zoomOutMax);
    }
    void MouseDown()
    {
        clickPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        raycast = Physics2D.Raycast(clickPoint, Vector2.zero, 1, 1 << 8 | 1 << 10);
        Static.isControlActive = true;
        if (raycast)
        {
            isRaycast = true;
            if (raycast.collider.CompareTag("Friend"))
            {
                currentFriend = raycast.transform.GetComponentInParent<Friend>();
                friend = true;
            }
            if (raycast.collider.CompareTag("Sponge"))
            {
                foreach (var v in scratch)
                {
                    v.InputEnabled = true;
                }
            }
            _rd = raycast.collider.GetComponent<Rigidbody2D>();
            if (!firstClick)
            {

                firstPoint = clickPoint;
                firstClick = true;

                
                offset = _rd.position - firstPoint;
            }
        }
        else
        {
            cameraClick = clickPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
    }

    void OnEnable()
    {
        Static.Reset_RD += ResetRD;
    }
    void OnDisable()
    {
        Static.Reset_RD -= ResetRD;
    }

    void ResetRD()
    {
        _rd = null;
        isRaycast = false;
        firstClick = false;
    }

    private int lowestY = 0;
    void MouseDrag()
    {
        clickPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (!isRaycast)
        {
            if (camera.orthographicSize < 8)
            {
                lowestY = -2;
            }
            else
            {
                lowestY = 0;
            }
            Vector3 direction = touchStart - Camera.main.ScreenToWorldPoint(Input.mousePosition);
            cameraTs.position += direction;
            cameraTs.transform.position = new Vector3(Mathf.Clamp(cameraTs.transform.position.x, leftClamp, rightClamp),
                Mathf.Clamp(cameraTs.transform.position.y, 0, 3f),
                Mathf.Clamp(cameraTs.transform.position.z, -5, -5));

        }
        if (isRaycast)
        {
            if (friend)
            {
                currentFriend.SetStatePhysics(false);
            }
            clickPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            float rotSpeed = 0.2f;
        }
    }



}
